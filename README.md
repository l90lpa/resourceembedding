## Resource Embedding

This is a simple resource embedding library inspired by [embed-resource](https://github.com/cyrilcode/embed-resource) with the primary focus of embedding OpenCL kernel files.

### Embedding resources in a project

This system uses python and CMake to generate a C++ source file (.cpp) for each input resource file containing a string representation of the. Then the stringified resource can be retrieved from else where in the project by use of the macro `LOAD_RESOURCE(x)`.

To embed resources (CMake side):  

- In an upper level CMakeLists.txt `add_subdirectory(ResourceEmbedding)`. This will make available the function "embedResources(...)" and add the CMake variable "ResourceEmbedding_INCLUDE_DIR" to the CMake variable cache.

- In the same CMakeLists.txt as the target that we want to embed the resources into call the function, `embedResources(output_resources file1 file2 ...)` where, each argument (file1, file2, ...) is a path to the resource relative to the current CMakeLists.txt and "output_resources" is a list of resource files to link into the project.

- To embed the resources add the output files to the target that we want to embed to. I.e. `add_executable(target_name ${output_resources} source_file1 source_file2 ...)`.

- Finally link the target to the header file "ResourceEmbed.h" in the ResourceEmbedding directory i.e. `target_link_libraries(target_name ResourceEmbedding OtherLibrary1 OtherLibrary2 ...)`.

To access embedded resources (C++ side):

- Add `#include "Resource.hpp"` to any file that the initial access/'load' of a resource should be done.

- Use the macro provided macro "LOAD_RESOURCE(x)" to load the resources as follows:
  - `Resource var_name = LOAD_RESOURCE(resource_name)`
  - Where:
    - var_name can be anything.
    - resource_name is the variable name used to store the embedded resource, this will be the original resource file name and file extension with any '.' replaced with '_'. I.e. `original/absolute/path/to/resourceFile.ext` will have resource_name `resourceFile_ext`.