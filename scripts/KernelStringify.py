import re, os, sys

def stringify(inputFilepath_rel, outputFilepath):
    # Input:
    # - inputFilepath_rel: file path relative to CMAKE_SOURCE_DIR
    # - outputFilepath: file path of output location

	inputFilepath = os.path.join(os.getcwd(), inputFilepath_rel)

    # Check that the input file exists and is of the correct type.
	if not os.path.exists(inputFilepath):
		print('Stringify: Input file does not exist.')
		return
	(dirName, fileName) = os.path.split(inputFilepath)
	if fileName == '':
		print('Stringify: The input filepath was to a directory.')
		return
	(name, ext) = os.path.splitext(fileName)

    # Format the input file line by line and write to the output file.
	formattedSource = ''
	with open(inputFilepath) as in_f:
		for line in in_f:
			result = re.search('//', line)
			if result:
				start = result.start()
				line = line[0:start] + '\n'
			
			line = re.sub('\"', '\\\"', line)
			line = re.sub('\\\\n', '\\\\\\\\n', line)
            #line = re.sub(';', '\;', line)

			lineLength = len(line)
			if lineLength != 0:
				if line[lineLength - 1] == '\n':
					line = '\"' + line[0:(lineLength - 1)] + '\\n\"\n'
				else:
					line = '\"' + line + '\"'
			else:
				line = '\"' + line + '\"'
            
			formattedSource += line
    
	if formattedSource.endswith('\n'):
		formattedSource = formattedSource[0 : len(formattedSource) - 1]
    
    # Open the output file.
	out_f = open(outputFilepath, 'w')

	# Form the variable name
	varName = re.sub('\.', '_', fileName)

	out_f.write("// This is an auto generated file. Ideally this shouldn\'t be edited but instead the generator should.\n\n")
	out_f.write('extern const char ' + varName + '[] = \n')
	out_f.write(formattedSource)
	out_f.write(';\n')
	
	sourceCodeLen = len(formattedSource)
	out_f.write('extern const size_t ' + varName + '_len = sizeof(' + varName + ');')
	out_f.close()


if __name__ == '__main__':
	print("Stringifying kernel.")
	if len(sys.argv) != 3:
		print('Stringify: ', len(sys.argv) - 1,' arguments passed requires only 1.')
	else:
		stringify(sys.argv[1], sys.argv[2])
