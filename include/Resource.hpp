// See: https://beesbuzz.biz/code/4399-Embedding-binary-resources-with-CMake-and-C-11

#ifndef RESOURCE_H
#define RESOURCE_H

#include <cstddef>
#include <string>

class Resource {
public:
	Resource(const char* start, const size_t len) : resource_data(start), data_len(len) {}
	Resource(const std::string& data) : resource_data{ data.c_str() }, data_len{ data.size() } {}


	const char * const &data() const { return resource_data; }
	const size_t &size() const { return data_len; }

	const char *begin() const { return resource_data; }
	const char *end() const { return resource_data + data_len; }

	std::string toString() { return std::string(data(), size()); }

private:
	const char* resource_data;
	const size_t data_len;

};

#define LOAD_RESOURCE(x) ([]() {                     \
        extern const char x[]; extern const size_t x##_len;   \
        return Resource(x, x##_len);  \
    })()

#endif