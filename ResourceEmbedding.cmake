cmake_minimum_required(VERSION 3.8)

set(resource_embedding_function_py_scripts_dir "${CMAKE_CURRENT_LIST_DIR}/scripts" CACHE INTERNAL "")

function(checkPythonIsAvailable)

# Finding python executable using CMake 3.12 onwards. 
#    find_package(Python3 COMPONENTS Interpreter)
#    if( !${Python3_FOUND} )
#        message( FATAL_ERROR "Python3 interpreter not found - needed for generating embedded resource files. Aborting process." )
#    endif()
#    set(python ${PYTHON_EXECUTABLE} PARENT_SCOPE)

# Finding python executable using 3.11 and backwards.

	find_package(PythonInterp)
	find_package(PythonLibs)

   if( !${PYTHONINTERP_FOUND} )
       message( FATAL_ERROR "Python3 interpreter not found - needed for generating embedded resource files. Aborting process." )
   endif()

   set(resource_embedding_function_py_exe_dir "${PYTHON_EXECUTABLE}" CACHE INTERNAL "")
   # The exe is at PYTHON_EXECUTABLE 
   # The version is at PYTHON_VERSION_STRING

endfunction(checkPythonIsAvailable)

function(embedResources output_resources)
	
    set(resource_list)
    foreach(in_f ${ARGN})
    
        file(RELATIVE_PATH src_f ${CMAKE_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/${in_f})

		get_filename_component(out_fname ${src_f} NAME)
		string(REPLACE "." "_" out_fname "${out_fname}")
		get_filename_component(out_fdir ${src_f} DIRECTORY)

		if("${out_fdir}" STREQUAL "")
			set(out_f "${PROJECT_BINARY_DIR}/${out_fname}.cpp")
		else()
			set(out_f "${PROJECT_BINARY_DIR}/${out_fdir}/${out_fname}.cpp")
		endif()
		message("out_f: " ${out_f})

        add_custom_command(OUTPUT ${out_f}
                           COMMAND ${resource_embedding_function_py_exe_dir} "${resource_embedding_function_py_scripts_dir}/kernelStringify.py" "${src_f}" "${out_f}"
                           DEPENDS "${in_f}"
                           WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                           COMMENT "Building binary file for embedding ${out_f}"
                           VERBATIM)

        list(APPEND resource_list "${out_f}")
    endforeach()
    set(${output_resources} "${resource_list}" PARENT_SCOPE)

endfunction(embedResources)

checkPythonIsAvailable()























#function(checkPythonIsAvailable python)
#
## Finding python executable using CMake 3.12 onwards. 
#    find_package(Python3 COMPONENTS Interpreter)
#    if( !${Python3_FOUND} )
#        message( FATAL_ERROR "Python3 interpreter not found - needed for generating embedded resource files. Aborting process." )
#    endif()
#    set(python ${PYTHON_EXECUTABLE} PARENT_SCOPE)
#
## Finding python executable using 3.11 and backwards.
##
##   if( !${PYTHONINTERP_FOUND} )
##       message( FATAL_ERROR "Python3 interpreter not found - needed for generating embedded resource files. Aborting process." )
##   endif()
##
## find_package(PythonInterp)
## find_package(PythonLibs)
## The exe is at PYTHON_EXECUTABLE 
## The version is at PYTHON_VERSION_STRING
#
#endfunction(check_pythonIsAvailable)
#
#function(embedResources python output_resources)
#
#    set(resource_list)
#    foreach(in_f ${ARGN})
#    
#        file(RELATIVE_PATH src_f ${CMAKE_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/${in_f})
#        set(out_f "${PROJECT_BINARY_DIR}/${in_f}.cpp")
#
#        add_custom_command(OUTPUT ${out_f}
#                           COMMAND ${python} ${out_f} ${src_f}
#                           DEPENDS ${in_f}
#                           WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
#                           COMMENT "Building binary file for embedding ${out_f}"
#                           VERBATIM)
#        list(APPEND resource_list "${out_f}")
#    endforeach()
#    set(${output_resources} "${resource_list}" PARENT_SCOPE)
#
#endfunction(embedResource)